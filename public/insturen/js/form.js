submitForm = function () {

    $('#form_submit').append(' <i id="submit_spinner" class="fas fa-circle-notch fa-spin"></i>');
    $('#form_submit').attr('.disabled');

    let formData = new FormData();

    // Append form submission to FormData
    formData.append("first_name", $("#first_name").val());
    formData.append("surname", $('#surname').val());
    formData.append("email", $('#email').val());
    formData.append("expertise", $('#expertise').val());
    formData.append("remarks", $('#remarks').val());
    formData.append("contest", $('#contest').val());
    formData.append("agreeCheck", $('#agreeCheck').prop("checked"));
   
    // Append the location data to formdata
    formData.append("lat", sessionStorage.getItem("lat"));
    formData.append("lng", sessionStorage.getItem("lng"));
    formData.append("zoom", sessionStorage.getItem("zoom"));

    // Append photo to formData
    formData.append('photo', document.getElementById('photo').files[0]);

    axios({
        method: 'post',
        url: '/submit',
        data: formData
    }).then(function (response) {
        console.log(response.data);
        if (response.data.succes) {
            // Succesfull entry
            $('#form_submit').removeAttr('.disabled');
            $('#submit_spinner').remove();
            $('#modal_success').modal('show');

            // Get the submitted bodemfoto's
            let data = {show: true, contest: 'bodemfoto'};

            axios({
                method: 'post',
                url: '/show',
                data: data
            }).then(function (response) {
                console.log(response.data);
                generateMap(response.data.results);
                $('#map').removeClass('d-none');
                $('#form').addClass('d-none');
                $('#modal_sucess_body').append( $("#first_name").val() + ', leuk dat je meedoet met Bodemfoto.nl! Je foto staat inmiddels op de kaart. Bekijk hem snel!');           
                $('.invalid-feedback').remove();
                $('.is-invalid').removeAttr('is-invalid');
            }).catch(function (err) {
                console.log(err);
            });
        } else {
            // Unsuccessfull entry            
            reportError(response.data.error);
        }
        
    }).catch(function (err) {
        console.log(err);
        $('#form_submit').removeAttr('.disabled');
        $('#submit_spinner').remove();
    });
};

reportError = function (error) {
    console.log(error[0]);
    if (error[0].errorAt == 'file') {
        // $('#photo').addClass('is-invalid');
        // $('#form_photo').append('<div class="invalid-feedback">' +
        //     error.message +
        //     '</div>'
        // );
        alert(error[0].message);
    } else {
        alert('Helaas, er iets fout gegaan. Probeer het later opnieuw');
    }
};