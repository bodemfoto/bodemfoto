function showDiv(selected_div) {
  if (selected_div == 'show_map') {
    $('#map').removeClass('d-none');
    $('#li_show_map').addClass('active');
    $('#info').addClass('d-none');
    $('#li_show_info').removeClass('active');
    $('#form').addClass('d-none');
    $('#terms').addClass('d-none');
    $('#privacy').addClass('d-none');
  } else if (selected_div == 'show_info') {
    $('#map').addClass('d-none');
    $('#li_show_map').removeClass('active');
    $('#info').removeClass('d-none');
    $('#li_show_info').addClass('active');
    $('#form').addClass('d-none');
    $('#terms').addClass('d-none');
    $('#privacy').addClass('d-none');
  } else if (selected_div == 'show_form') {
    $('#map').addClass('d-none');
    $('#li_show_map').removeClass('active');
    $('#info').addClass('d-none');
    $('#li_show_info').removeClass('active');
    $('#form').removeClass('d-none');
    $('#terms').addClass('d-none');
    $('#privacy').addClass('d-none');
  } else if (selected_div == 'show_terms') {
    $('#map').addClass('d-none');
    $('#li_show_map').removeClass('active');
    $('#info').addClass('d-none');
    $('#li_show_info').removeClass('active');
    $('#form').addClass('d-none');
    $('#terms').removeClass('d-none');
    $('#privacy').addClass('d-none');
  } else if (selected_div == 'show_privacy') {
    $('#map').addClass('d-none');
    $('#li_show_map').removeClass('active');
    $('#info').addClass('d-none');
    $('#li_show_info').removeClass('active');
    $('#form').addClass('d-none');
    $('#terms').addClass('d-none');
    $('#privacy').removeClass('d-none');
  }
}

$(document).ready(function () {
  $('#sidebarCollapse').on('click', function () {
      $('#sidebar').toggleClass('active');
  });

  // Add submit button to form
  $('#div_submit').append('<button id="form_submit" type="submit" class="btn btn-info">Submit</button>');
  $('#form_bodemfoto').on('submit',function(e) {
    e.preventDefault();
    submitForm();
  });

  //Navigation functions
  $('#show_map').on('click', function () {
    showDiv('show_map');    
  });
  $('#show_info').on('click', function () {
    showDiv('show_info');
  });
  $('#show_terms').on('click', function () {
    showDiv('show_terms');
  });
  $('#show_privacy').on('click', function () {
    showDiv('show_privacy');
  });

  // Get the submiited bodemfoto's
  let data = {show: true, contest: 'bodemfoto'};

  axios({
      method: 'post',
      url: '/show',
      data: data
  }).then(function (response) {
    console.log(response.data);
    generateMap(response.data.results);
  }).catch(function (err) {
      console.log(err);
  });
});