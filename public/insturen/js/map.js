function deg_to_dms (deg, axis) {
    let d = Math.floor (deg);
    let minfloat = (deg-d)*60;
    let m = Math.floor(minfloat);
    let secfloat = (minfloat-m)*60;
    let s = Math.round(secfloat);                
    if (s==60) {
        m++;
        s=0;
    }
    if (m==60) {
        d++;
        m=0;
    }
    if (axis=="lat") {
        if (deg<0) {
            return ("" + d + "° " + m + "′  " + s + "'' Z");
        } else if (deg>=0) {
             return ("" + d + "° " + m + "′  " + s + "'' N");
        }
    } else if (axis=="lng") {
        if (deg<0) {
            return ("" + d + "° " + m + "′  " + s + "'' W");
        } else if (deg>=0) {
             return ("" + d + "° " + m + "′  " + s + "'' O");
        }    
    }                
}


function showDetails(entry) {
    console.log(entry);

    // Remove previous entry
    $('#select').empty();
    $('#select').append(
        '<a id="btn_backToMap" class="btn btn-info"><span>Terug naar de kaart</span></a>' +
        '<h2>De bodemfoto van '+ entry.first_name +'</h2>' +
        '<p>'+ deg_to_dms(entry.lat, "lat") +', '+ deg_to_dms(entry.lng, "lng") +'</p>' +
        '<img src="uploads/original/'+ entry.file_name +'" class="img-fluid" alt="De bodemfoto van '+ entry.first_name +'"></img>' +
        '<p>'+ entry.remarks +'</p>'
    );

    $('#map').addClass('d-none');
    $('#select').removeClass('d-none');

    // When clicked backToMap go back
    $('#btn_backToMap').on('click', function () {
        $('#map').removeClass('d-none');
        $('#select').addClass('d-none');
    });
    
}

function generateMap(results) {
    // Remove previous map
    $('#map').remove();
    $('#main').append('<div id="map"></div>');

    // Create new map
    let map = new L.Map('map');

    let osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	let osmAttrib='Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
    let osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 12, attribution: osmAttrib});
    
    map.setView(new L.LatLng(52.02, 5.60),5);
    map.addLayer(osm);

    let popup = L.popup();
    function clickMap(e) {
        popup
        .setLatLng(e.latlng)
        .setContent('<p>Is dit je locatie?</p>' +
                    '<a id="btn_showForm" class="btn btn-info">Yep!</a')
        .openOn(map);

        $('#btn_showForm').on('click', function () {
            console.log('hoi');
            showDiv('show_form');
        });

        sessionStorage.lat = e.latlng.lat;
        sessionStorage.lng = e.latlng.lng;
        sessionStorage.zoom = map.getZoom();
    }
    
    map.on('click', clickMap);

    // Put the photo on the map
    console.log(results);
    results.forEach(function (entry) {
        let icon = L.icon({
            iconUrl: 'uploads/thumbs/' + entry.file_name,
            // iconSize: [30, 30],
            iconAnchor: [15, 15], 
            popupAnchor:  [0, 15] 
        });

        let marker = L.marker(
                [entry.lat, entry.lng],
                {icon: icon})
            .bindTooltip('<p>De bodemfoto van: ' + entry.first_name + '</p>').openTooltip()
            .addTo(map)
            .on('click', function () {
                showDetails(entry);
            });
    });
}
