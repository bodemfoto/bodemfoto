# Bodemfoto.nl

Bodemfoto.nl is a project to increase the awareness of the Dutch public to their soils they live on. The idea is that people send pictures of soils they meet. That can be pictures of a classic soil profile, but also a soil revealed at a construction site or by erosion. The pictures are presented on a map at the website and in this way we collect overview of Dutch soils and how people experience them.

Take a look at www.bodemfoto.nl !

## More info
Do you have any ideas, suggestions or contributions? Please submit them or contact us at bodemfoto@gmail.com.

## Installation
1.  Install [Node.js](https://nodejs.org/en/download/package-manager/)
2.  Install [MariaDB](https://downloads.mariadb.org/mariadb/repositories/#mirror=weppel) and create an user to be used for bodemfoto
3.  Clone this repo using `git clone git@gitlab.com:bodemfoto/bodemfoto.git` 
4.  Move to newly created folder bodemfoto and run `npm install`
5.  Import `database/bodemfoto_structure.sql` into MariaDB
6.  Create in the root folder of bodemfoto a `.env` with the following content:
```
ENVIRONMENT = production
DB_USER = [YOUR_USERNAME]
DB_PASS = [YOUR_PASSWORD]
DB_NAME = bodemfoto
DB_HOST = localhost
```
7.  Run `npm start` to start the server

## Color scheme
https://material.io/tools/color/#!/?view.left=0&view.right=0&primary.color=78909C&secondary.color=A1887F
