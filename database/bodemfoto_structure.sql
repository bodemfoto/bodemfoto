-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Gegenereerd op: 31 okt 2018 om 11:21
-- Serverversie: 5.7.24-0ubuntu0.18.04.1
-- PHP-versie: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bodemfoto`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `entries`
--
--

CREATE TABLE `entries` (
  `ID` int(11) NOT NULL,
  `uuid` varchar(36) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `first_name` varchar(64) NOT NULL,
  `surname` varchar(64) DEFAULT NULL,
  `email` varchar(64) NOT NULL,
  `expertise` set('expert','amateur') NOT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `contest` varchar(64) NOT NULL,
  `agreeCheck` varchar(4) NOT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  `zoom` int(2) NOT NULL,
  `file_name_original` varchar(100) NOT NULL,
  `file_name` varchar(42) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `entries`
--
ALTER TABLE `entries`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `uuid` (`uuid`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `entries`
--
ALTER TABLE `entries`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
