const bodyParser = require('body-parser');
const express = require('express');
const multer  = require('multer');
const helmet = require('helmet');
const uuidv4 = require('uuid/v4');
const Jimp = require('jimp');
require('dotenv').config();
require('./back-end/validation.js')();
require('./back-end/db_functions.js')();
const app = express();

// Set up helmet
app.use(helmet());

// Serve up content from public directory
app.use(express.static(__dirname + '/public'));

// Set up body-parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Set port
let port = null;
if (process.env.ENVIRONMENT ==  "development") {
  port  = 3000;
} else if (process.env.ENVIRONMENT == "production") {
  port = 80;
} else {
  console.log('Choose the right ENVIRONMENT  in `.env`');
}

// Set up upload of picture
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __dirname + '/public/insturen/uploads/original/');
  },
  filename: function (req, file, cb) {
    req.body.file_name_original = file.originalname;
    let uuid = uuidv4();
    req.body.uuid = uuid;
    req.body.file_name =  uuid + '.jpg';
    cb(null, req.body.file_name);
  }
});
let upload = multer({ storage: storage,
  limits: {fileSize: 10000000}, //10 MB
  fileFilter: function(req, file, cb) {
    console.log(file.mimetype);  
    if (file.mimetype !== 'image/png' & file.mimetype !== 'image/jpeg') {
      req.fileValidationError = {errorAt: 'file', message: 'Alleen jpg en png zijn toegestaan'};
      return cb(null, false, new Error('goes wrong on the mimetype'));
    } else {
      cb(null, true);
    }   
  }
}).single('photo'); 

// Run this if a entry is submitted
app.post('/submit', (req, res) => {
  upload(req, res, function (err) { 
    console.log(req.fileValidationError);
    if (req.fileValidationError) {
      res.send(req.fileValidationError);      
    } else if (err) {
      if (err instanceof multer.MulterError) {
        console.log(err);
        res.send({'message': 'Multer error'});
      } else {
        console.log(err);
        res.send({'message': 'error'});
      }
    } else if (req.body.photo == "undefined") {
      console.log('hoi');
      let response = {'success': false, 'error': []};
      response.error.push({errorAt: 'file', message: 'Je hebt geen foto geselecteerd'});
      res.send(response);    
    } else {
      // Create a thumbnail
      Jimp.read( __dirname + '/public/insturen/uploads/original/' + req.body.file_name)
      .then(photo => {
        return photo
          .resize(Jimp.AUTO, 30) // resize to 30px height
          .write(__dirname + '/public/insturen/uploads/thumbs/' + req.body.file_name); // save
      })
      .catch(err => {
        console.error(err);
      });

      // Send entry to db
      submit(req.body) 
      .then(response => {
        console.log(response);
        res.send(response);
      }).catch( err => {
        console.log(err);
        res.send(err);
      });
    }
  }); 
});

// Select the pictures from the database
app.post('/show', (req, res) => {
   select_photos(req.body)
    .then(response => {
      res.send(response);
    }).catch(err => {
      res.send(err);
    });
});

app.listen(port);
console.log("Bodemfoto is running in " + process.env.ENVIRONMENT + " mode at port " + port);
