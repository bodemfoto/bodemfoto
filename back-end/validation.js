module.exports = function () {
    this.validateForm = function (data) {
        let response = {'success': true, 'error': []};

        return new Promise (function (resolve, reject) {

            // Validate first name
            if (data.first_name == "") {
                response.success = false;
                response.error.push({errorAt: 'first_name', message: 'Vul je voornaam in'});
            } else if (data.first_name.length < 3) {
                response.success = false;
                response.error.push({errorAt: 'first_name', message: 'Je voornaam moet uit minimaal 2 letters bestaan'});
            } else if (data.first_name.length > 64) {
                response.success = false;
                response.error.push({errorAt: 'first_name', message: 'Helaas, je voornaam kan niet langer zijn dan 64 tekens'});
            }

            // Validate surname
            if (data.surname == "") {
                response.success = false;
                response.error.push({errorAt: 'surname', message: 'Vul je voornaam in'});
            } else if (data.surname.length < 3) {
                response.success = false;
                response.error.push({errorAt: 'surname', message: 'Je achternaam moet uit minimaal 2 letters bestaan'});
            } else if (data.surname.length > 64) {
                response.success = false;
                response.error.push({errorAt: 'surname', message: 'Helaas, je achternaam kan niet langer zijn dan 64 tekens'});
            }

            // Validate e-mail
            const regex_email = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
            if (data.email == "") {
                response.success = false;
                response.error.push({errorAt: 'email', message: 'Vul je e-mailadres in'});
            } else if (! regex_email.test(data.email)) {
                response.success = false;
                response.error.push({errorAt: 'email', message: 'Vul een geldig e-mailadres in'});
            } else if (data.email.length > 64) {
                response.success = false;
                response.error.push({errorAt: 'email', message: 'Helaas, je e-mailadres kan niet langer zijn dan 64 tekens'});
            }

            // Validate photo
            if (data.photo == 'undefined') {
                response.success = false;
                response.error.push({errorAt: 'file', message: 'Je hebt geen foto geselecteerd'});
            }

            // Validate expertise
            if (data.expertise == '') {
                response.success = false;
                response.error.push({errorAt: 'expertise', message: 'Je bent vergeten je ervaring met het thema bodem te selecteren'});
            } else if (data.expertise != 'expert' & data.expertise != 'amateur') {
                response.success = false;
                response.error.push({errorAt: 'expertise', message: 'Helaas, je moet 1 van de 2 opties kiezen bij je ervaring met bodem'});
            }

            // Validate remarks
            if (data.remarks.length > 1000) {
                response.success = false;
                response.error.push({errorAt: 'remarks', message: 'Helaas, je opmerking mag niet langer zijn dan 1000 tekens'});
            }

            // Validate agreeCheck
            if (! data.agreeCheck || data.agreeCheck == "false") {
                response.success = false;
                response.error.push({errorAt: 'agreeCheck', message: 'Helaas, je kan alleen meedoen als je akkoord gaat met de voorwaarden'});
            }

            console.log(response);
            // Check if there are errors and give a response
            if (response.succes) {
                resolve(response);
            } else {
                return reject(response);
            }
        });
    };
};