const mysql = require('mysql2');
class Database {
    constructor(config) {
        this.pool = mysql.createPool(config);       
    }
    execute(sql, args) {
        return new Promise((resolve, reject) => {
            this.pool.execute(sql, args, (err, rows) => {
                if (err) {
                    return reject(err);
                }
                resolve(rows);
            });
        });
    }
}

getDBlogin = function () {
    require('dotenv').config();
    
    let DBlogin = {
        host : process.env.DB_HOST,
        user : process.env.DB_USER,
        password : process.env.DB_PASS,
        database : process.env.DB_NAME,
        waitForConnections : true,
        connectionLimit : 10,
        queueLimit: 5
    };

    return(DBlogin);
};

module.exports = function () {
    this.submit = function (data) {
        return new Promise (function (resolve, reject) {
            console.log(data);
            validateForm(data)
             .then(response => {
                console.log(response);
                if (response.success) {
                    // Set up a response object
                    let response = {'success': true, 'error': []};
                    // Insert to DB
                    const db = new Database(getDBlogin());
                    db.execute('INSERT INTO `entries` (`uuid`, `first_name`, `surname`, `email`, `expertise`, `remarks`, `contest`, `agreeCheck`, `lat`, `lng`, `zoom`, `file_name_original`, `file_name`)' +
                        'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                        [data.uuid, data.first_name, data.surname, data.email, data.expertise, data.remarks, data.contest, data.agreeCheck, data.lat, data.lng, data.zoom, data.file_name_original, data.file_name])
                    .then(results => {                
                        response.success = true;
                        resolve(response);
                    }).catch(err => {
                        console.log(err);
                        response.success = false;
                        response.error.push({errorAt: 'database', message: 'Helaas, er is iets misgegaan met je inzending. Probeer later opnieuw'});
                        return reject(response);
                    });
                } else {
                    return reject(response);
                }      
             }).catch(err => {
                console.log(err);
                return reject(err);
             });                  
        });
    };
    this.select_photos = function (data) {
        return new Promise (function (resolve, reject) {
            console.log(data);

            // Set up a response object
            let response = {'success': null, 'results': null, 'error': null};

            // Select from DB
            const db = new Database(getDBlogin());
            db.execute('SELECT `uuid`, `first_name`, `remarks`, `lat`, `lng`, `file_name` FROM `entries` WHERE `contest` IN (?)',
                [data.contest])
            .then(results => {
                response.results = results;
                response.success = true;
                resolve(response);
            }).catch(err => {
                console.log(err);
                response.success = false;
                response.error = err;
                return reject(response);
            });
        });
    };
};